'use strict'
const app=require('./app');
const port = process.env.PORT || 2000;
const Sequelize=require('sequelize');
const env=require('./config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
   host:env.host,
    port:env.port,
    username:env.username,
    password:env.password,
    database:env.database,
    dialect:env.dialect
});

sequelize
    .authenticate()
    .then(()=>{

       app.listen(port,function () {
           console.log('Conectado a MSSQLS ' + port);
       });
    })
    .catch(err=>{
        console.log('No se puede conectar a MSSQLS ', err);
    });

    module.exports=sequelize;

   





