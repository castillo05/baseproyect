'use strict'
const express=require('express');
const FormatoController=require('../controllers/formato');
const mdauth = require('../middleware/authenticated');

var api=express.Router();

api.get('/formato/getformatos',mdauth.ensureAuth,FormatoController.getFormatos);



module.exports=api;