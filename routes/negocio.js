'use strict'
const express=require('express');
const NegocioController=require('../controllers/negocio');
const mdauth = require('../middleware/authenticated');

var api=express.Router();

api.get('/negocio/getnegocios',mdauth.ensureAuth,NegocioController.getNegocios);
api.get('/negocio/getnegocio/:id',mdauth.ensureAuth,NegocioController.getNegocio);
api.post('/negocio/negociocreate/',mdauth.ensureAuth,NegocioController.negocioCreate);
api.put('/negocio/updatenegocio/:id',mdauth.ensureAuth,NegocioController.negocioUpdate);


module.exports=api;