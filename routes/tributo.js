'use strict'
const express=require('express');
const TributoController=require('../controllers/tributo');
const mdauth = require('../middleware/authenticated');

var api=express.Router();

api.get('/tributo/gettributos',mdauth.ensureAuth,TributoController.getTributos);



module.exports=api;