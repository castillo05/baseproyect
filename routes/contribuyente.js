'use strict'
const express=require('express');
const ContribuyenteController=require('../controllers/contribuyente');
const mdauth = require('../middleware/authenticated');

var api=express.Router();

api.get('/contribuyente/getcontribuyentes',mdauth.ensureAuth,ContribuyenteController.getContribuyentes);
api.post('/contribuyente/contribuyentestored',mdauth.ensureAuth,ContribuyenteController.contribuyenteStored);
api.get('/contribuyente/getcontribuyente/:id',mdauth.ensureAuth,ContribuyenteController.getContribuyente);

module.exports=api;