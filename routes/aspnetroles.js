'use strict'

const express=require('express');
const AspnetrolesController=require('../controllers/aspnetroles');
const mdAuth=require('../middleware/authenticated');

const api=express.Router();

api.post('/roles/rolestored',mdAuth.ensureAuth,AspnetrolesController.RoleStored);

module.exports=api;