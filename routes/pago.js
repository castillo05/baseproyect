'use strict'
const express=require('express');
const PagoController=require('../controllers/pago');
const mdauth = require('../middleware/authenticated');

var api=express.Router();

api.get('/pago/getpago/:id',mdauth.ensureAuth,PagoController.getPago);
api.post('/pago/pagocreate',mdauth.ensureAuth,PagoController.pagoCreate);
api.put('/pago/pagoupdate/:id_pago',mdauth.ensureAuth,PagoController.pagoUpdate);
api.delete('/pago/pagodelete/:id',mdauth.ensureAuth,PagoController.pagoDelete);

module.exports=api;