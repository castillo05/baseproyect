'use strict'
const express= require('express');
const ParcelasController= require('../controllers/parselas');

const mdauth=require('../middleware/authenticated');

var api=express.Router();

api.post('/parcela/parcelacreate',mdauth.ensureAuth,ParcelasController.parcelaCreate);
api.get('/parcela/getparcelas/:id',mdauth.ensureAuth,ParcelasController.getParcelas);
api.get('/parcela/getparcela/:id',mdauth.ensureAuth,ParcelasController.getParcela);

module.exports=api;