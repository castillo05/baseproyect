'use strict'
const express=require('express');
const ActividadController=require('../controllers/actividad');
const mdauth = require('../middleware/authenticated');

var api=express.Router();

api.get('/actividad/getactividades',mdauth.ensureAuth,ActividadController.getActividades);



module.exports=api;