'use strict'
const express=require('express');
const DireccionController=require('../controllers/direccion');
const mdauth = require('../middleware/authenticated');

var api=express.Router();

api.get('/direccion/getdireccion',mdauth.ensureAuth,DireccionController.getDireccion);



module.exports=api;