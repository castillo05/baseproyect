'use strict'
const express=require('express');
const AspnetusersController=require('../controllers/aspnetusers');

const api=express.Router();

api.post('/usuario/userstored',AspnetusersController.UserStored);
api.post('/usuario/singin',AspnetusersController.SingIn);
api.get('/usuario/getusers',AspnetusersController.GetUsers);
api.put('/usuario/userupdate/:Id_User',AspnetusersController.UserUpdate);
api.get('/usuario/getuser/:Id_User',AspnetusersController.GetUser);

module.exports=api;  