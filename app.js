'use strict'
const express = require('express');
const BodyParser=require('body-parser');

const app = express();

// Exportamos rutas
const ContribuyentesRoute=require('./routes/contribuyente');
const AspnetusersRoute=require('./routes/aspnetusers');
const AspnetrolesRoute=require('./routes/aspnetroles');
const NegociosRoute=require('./routes/negocio');
const DireccionRoute=require('./routes/direccion');
const PagoRoute=require('./routes/pago');
const FormatoRoute=require('./routes/formatos');
const ActividadRoute=require('./routes/actividad')
const TributoRoute=require('./routes/tributo');
const ParcelasRoute=require('./routes/parcelas');

app.use(BodyParser.urlencoded({extended:false,limit:'50mb',parameterLimit:50000}));
app.use(BodyParser.json({limit:'50mb'}));

app.use((req,res,next) =>
{
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Headers','Authorization, X-API-KEY,Origin,X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request');
    res.header('Access-Control-Allow-Methods','GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// Rutas para Contribuyentes
app.use('/api',ContribuyentesRoute);

// Rutas para Usuarios
app.use('/api',AspnetusersRoute);

// Rutas para Roles
app.use('/api',AspnetrolesRoute);

// Rutas para Negocios
app.use('/api',NegociosRoute);

//Rutas para direcciones
app.use('/api',DireccionRoute);

//Rutas de pagos
app.use('/api',PagoRoute);

//Rutas de Formatos
app.use('/api',FormatoRoute);

//Rutas de Actividad
app.use('/api',ActividadRoute);

//Rutas para tributos
app.use('/api',TributoRoute);

// Rutas para Parcelas
app.use('/api',ParcelasRoute);

app.get('/',function(req,res){
    res.status(200).send('Bienvenido a Sequelize Test');
});

module.exports=app;

