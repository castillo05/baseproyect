'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'Alcald1aelsauce';

exports.createToken=function (user) {
    var payload={
        sub:user.id,
        Email:user.Email,
        UserName:user.UserName,
        Name:user.Name,
        LastName:user.LastName,
        Cedula:user.Cedula,
        iat:moment().unix(),
        exp:moment().add(30,'days').unix

    }
    return jwt.encode(payload,secret);
}