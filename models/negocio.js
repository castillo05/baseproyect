'use strict'
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

const Negocio = sequelize.define('Negocio',{
    id_n:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    nombre_n:Sequelize.STRING,
    num_mat:Sequelize.INTEGER,
    fecha_n:{type:Sequelize.DATE,defaultValue:Sequelize.NOW},
    id_cont_negocio:Sequelize.INTEGER,
    id_direccion:Sequelize.INTEGER,
    id_actividad:Sequelize.INTEGER,
    id_tributo:Sequelize.INTEGER,
    estado:Sequelize.STRING
});



module.exports=Negocio;