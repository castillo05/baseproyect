'use strict'
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

const Actividad = sequelize.define('Actividad',{
    id_actividad:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    actividad:Sequelize.STRING
   
});



module.exports=Actividad;