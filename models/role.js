'use strict'
const db=require('../index');
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

 const AspNetRoles= sequelize.define('AspNetRoles',{
    Id:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    Name:Sequelize.STRING,
    Enable:Sequelize.INTEGER,
    Discriminator:Sequelize.STRING
 });

 

 module.exports=AspNetRoles;