'use strict'
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

 const Aspnetusers=sequelize.define('AspNetUsers',{
    Id_User:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    Mail:Sequelize.STRING,
    PasswordHash:Sequelize.STRING,
    PhoneNumber:Sequelize.INTEGER,
    LockoutEnabled:Sequelize.INTEGER,
    UserName:Sequelize.STRING,
    Name:Sequelize.STRING,
    LastName:Sequelize.STRING,
    Cedula:Sequelize.STRING,
    RoleId:Sequelize.INTEGER
 });



 module.exports=Aspnetusers;