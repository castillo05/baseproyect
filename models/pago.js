'use strict'
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

const Pago = sequelize.define('Pago',{
    id_pago:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    concepto:Sequelize.STRING,
    id_negocio:Sequelize.INTEGER,
    fecha:{type:Sequelize.DATE,defaultValue:Sequelize.NOW},
    id_colector:Sequelize.INTEGER,
    tipo_pago:Sequelize.STRING,
    monto:Sequelize.FLOAT,
    total_pago:Sequelize.FLOAT,
    id_formato:Sequelize.INTEGER,
    id_tributo:Sequelize.INTEGER
});



module.exports=Pago;