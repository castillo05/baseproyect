'use strict'
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

const Tributo = sequelize.define('Tributo',{
    id_tributo:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    nombre_t:Sequelize.STRING,
    codigo_t:Sequelize.INTEGER
   
});



module.exports=Tributo;