'use strict'
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

const Contribuyente = sequelize.define('Contribuyente',{
    id:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    nombre_c:Sequelize.STRING,
    apellido_c:Sequelize.STRING,
    cedula_c:Sequelize.STRING,
    direccion_c:Sequelize.STRING,
    fecha_ing_c:{type:Sequelize.DATE,defaultValue:Sequelize.NOW},
    telefono:Sequelize.INTEGER,
    email:Sequelize.STRING
});



module.exports=Contribuyente;