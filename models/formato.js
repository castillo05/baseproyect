'use strict'
const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

const Formato = sequelize.define('Formato',{
    id_formato:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
    nombre_f:Sequelize.STRING,
    valor:Sequelize.FLOAT,
   
});



module.exports=Formato;