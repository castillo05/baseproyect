'use strict'

const Sequelize=require('sequelize');
const env=require('../config');
const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

 const Parcela=sequelize.define('Parcela',{
     id_parcela:{type:Sequelize.INTEGER,primaryKey:true,autoIncrement:true},
     id_contribuyente:Sequelize.INTEGER,
     anio_pagado:Sequelize.INTEGER,
     cod_siscat:Sequelize.STRING,
     jubilado:Sequelize.STRING,
     descuento:Sequelize.FLOAT,
     total:Sequelize.FLOAT,
     pago1:Sequelize.FLOAT,
     pago2:Sequelize.FLOAT,
     saldo:Sequelize.FLOAT,
     nuevo:Sequelize.STRING,
     aviso:Sequelize.INTEGER,
     fecha_notificacion:Sequelize.STRING,
     fecha_pago:Sequelize.STRING,
     pn:Sequelize.STRING,
     tipo_localidad:Sequelize.STRING,
     barrio:Sequelize.INTEGER,
     caserio:Sequelize.INTEGER
 })

 module.exports=Parcela;