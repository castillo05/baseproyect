'use strict'
const Sequelize=require('sequelize');
const Contribuyente=require('../models/contribuyente');
const Pago=require('../models/pago');
const env=require('../config');
const Direccion= require('../models/direccion');

//Asociaciones
// Negocio.hasMany(Pago,{foreignKey:'id_negocio'});
// Pago.belongsTo(Negocio,{foreignKey:'id_negocio'});

const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });
function getPago(req,res){
      let id=req.params.id;
      Pago.findOne({
          where:{
              id_pago:id
          }
      }).then(p=>{
          res.status(200).send({pago:p});
      }).catch(err=>{
          console.log(err);
      })
}

function pagoCreate(req,res){
            var params=req.body;
            if (params.concepto=='' 
            || params.id_colector==''
            || params.id_negocio==''
            || params.tipo_pago=='' 
            || params.monto=='' 
            || params.total_pago==''
            || params.id_formato=='' 
            || params.id_tributo=='') {
                res.status(200).send({message:'Llenar todos los campos!'});
                
            } else {
                
            
                Pago.create({
                    concepto:params.concepto,
                    id_negocio:params.id_negocio,
                    //fecha:date,
                    id_colector:params.id_colector,
                    tipo_pago:params.tipo_pago,
                    monto:params.monto,
                    total_pago:params.total_pago,
                    id_formato:params.id_formato,
                    id_tributo:params.id_tributo
                }).then(pagoStored=>{
                    res.status(200).send({pagoStored:pagoStored});
                }).catch(err=>{
                    res.status(500).send({err});
                })
            }
}

function pagoUpdate(req,res) {
    var params=req.body;
    if (params.concepto=='' 
    || params.id_colector=='' 
    || params.fecha=='' 
    || params.id_negocio==''
    || params.tipo_pago=='' 
    || params.monto=='' 
    || params.total_pago==''
    || params.id_formato=='' 
    || params.id_tributo=='') {
        res.status(200).send({message:'Llenar todos los campos!'});
        
    } else {
        
    
        Pago.update({
            concepto:params.concepto,
            id_negocio:params.id_negocio,
            //fecha:date,
            id_colector:params.id_colector,
            tipo_pago:params.tipo_pago,
            monto:params.monto,
            total_pago:params.total_pago,
            id_formato:params.id_formato,
            id_tributo:params.id_tributo
        },{
            where:{
                id_pago:req.params.id_pago
            }
        }).then(pagoUpdate=>{
           if(pagoUpdate) return res.status(200).send({message:'Pago Actualizado Satisfactoriamente'});

          
        }).catch(err=>{
            res.status(500).send({err});
        })
    }
}

function pagoDelete(req,res) {
    let id=req.params.id;
    Pago.destroy({
        where:{
            id_pago:id
        }
    }).then(pagoDelete=>{
        if(pagoDelete) return res.status(200).send({message:'Pago Eliminado Satisfactoriamente'});
    }).error(err=>{
        res.status(500).send({err});
    })
}

// function getNegocio(req,res){
//     let id=req.params.id;
//     sequelize.sync().then(()=>{
//         Negocio.findOne({
//              where:{
//             id_n:id
//             }
//             ,include:[{
//                 model:Pago,
//                 as:''
//             }]
//         }).then(n=>{
//             res.status(200).send({negocio:n});
//         }).catch(error=>{
//             res.status(404).send({Error:error});
//         });
       
//     })
// }
   
module.exports={
    getPago,
    pagoCreate,
    pagoUpdate,
    pagoDelete
}
