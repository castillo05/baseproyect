'use strict'
const Sequelize=require('sequelize');
const Contribuyente=require('../models/contribuyente');
const Pago=require('../models/pago');
const env=require('../config');
const Direccion= require('../models/direccion');

//Asociaciones
// Negocio.hasMany(Pago,{foreignKey:'id_negocio'});
// Pago.belongsTo(Negocio,{foreignKey:'id_negocio'});

const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });
function getDireccion(req,res){
  
      Direccion.findAll().then(d=>{
          res.status(200).send({direccion:d});
      }).catch(err=>{
          console.log(err);
      })
}

// function getNegocio(req,res){
//     let id=req.params.id;
//     sequelize.sync().then(()=>{
//         Negocio.findOne({
//              where:{
//             id_n:id
//             }
//             ,include:[{
//                 model:Pago,
//                 as:''
//             }]
//         }).then(n=>{
//             res.status(200).send({negocio:n});
//         }).catch(error=>{
//             res.status(404).send({Error:error});
//         });
       
//     })
// }
   
module.exports={
    getDireccion
}
