'use strict'
const Sequelize=require('sequelize');
const Contribuyente=require('../models/contribuyente');
const Negocio=require('../models/negocio');
const Pago=require('../models/pago');
const env=require('../config');

//Asociaciones
Negocio.hasMany(Pago,{foreignKey:'id_negocio'});
Pago.belongsTo(Negocio,{foreignKey:'id_negocio'});

const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });
function getNegocios(req,res){
  
      Negocio.findAll().then(n=>{
          res.status(200).send({negocios:n});
      }).catch(err=>{
          console.log(err);
      })
}

function getNegocio(req,res){
    let id=req.params.id;
    sequelize.sync().then(()=>{
        Negocio.findOne({
             where:{
            id_n:id
            }
            ,include:[{
                model:Pago,
                as:''
            }]
        }).then(n=>{
            res.status(200).send({negocio:n});
        }).catch(error=>{
            res.status(404).send({Error:error});
        });
       
    })
}

function negocioCreate(req,res) {
    let params=req.body;
    let estado='HABILITADO'
    if (params.nombre_n=='' || params.id_cont_negocio=='' || params.id_direccion=='' || params.id_actividad=='' || params.id_tributo=='') {
        res.status(200).send({message:'Llenar todos los campos!'});
        
    } else {
        Negocio.create({
            nombre_n:params.nombre_n,
            num_mat:params.num_mat,
            id_cont_negocio:params.id_cont_negocio,
            id_direccion:params.id_direccion,
            id_actividad:params.id_actividad,
            id_tributo:params.id_tributo,
            estado:estado
        }).then(negocioStored=>{
            res.status(200).send({negocio:negocioStored});
        }).catch(err=>{
            res.status(500).send({error:err});
        })
    }

}


function negocioUpdate(req,res){
    let id = req.params.id;
    let params=req.body;
    if (params.nombre_n=='' || params.id_cont_negocio=='' || params.id_direccion=='' || params.id_actividad=='' || params.id_tributo=='') {
        res.status(200).send({message:'Llenar todos los campos!'});
        
    }else{
       Negocio.update({
            nombre_n:params.nombre_n,
            num_mat:params.num_mat,
            id_cont_negocio:params.id_cont_negocio,
            id_direccion:params.id_direccion,
            id_actividad:params.id_actividad,
            id_tributo:params.id_tributo,
            estado:params.estado

       },{
           where:{
               id_n:id
           }
       }).then(negocioUpdate=>{
           if(negocioUpdate) return res.status(200).send({message:'Negocio Actualizado Satisfactoriamente'});
       }).catch(err=>{
        res.status(500).send({error:err});
       })
    }

}
   
module.exports={
    getNegocios,
    getNegocio,
    negocioCreate,
    negocioUpdate
}
