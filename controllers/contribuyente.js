'use strict'
const Sequelize=require('sequelize');
const Contribuyente=require('../models/contribuyente');
const Negocio=require('../models/negocio');
const env=require('../config');

//Asociaciones
Contribuyente.hasMany(Negocio,{foreignKey:'id_cont_negocio'});
Negocio.belongsTo(Contribuyente,{foreignKey:'id_cont_negocio'});


const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });
function getContribuyentes(req,res){
    
    sequelize.sync().then(()=>{
        Contribuyente.findAll({
            include:[{
                model:Negocio,
                as:''
            }]
        }).then(c=>{
            res.status(200).send({contribuyente:c});
        }).catch(err=>{
            res.status(404).send({Error:err});
        })
    })

   
}

function getContribuyente(req,res){
    let id=req.params.id;
    sequelize.sync().then(()=>{
        Contribuyente.findOne({
            where:{
                id:id
            },
            include:[{
                model:Negocio,
                as:''
            }]
        }).then(c=>{
            res.status(200).send({contribuyente:c});
        }).catch(error=>{
            res.status(404).send({Error:err});
        })
    })
}

function contribuyenteStored(req,res){
    var params = req.body;

    //Validamos los campos
    if (params.nombre_c=='' || params.apellido_c=='' || params.cedula_c=='' || params.direccion_c=='') {
        res.status(200).send({message:'Llenar todos los campos!'});
    }else{
        //Buscamos el numero de cedula
        Contribuyente.findAll({
            where:{
                cedula_c:params.cedula_c
            }
        }).then(cont=>{
            if(cont.length>=1){
                res.status(200).send({message:'Lo sentimos este Contribuyente ya esta registrado'});

            }else{
                Contribuyente.create({
                    nombre_c:params.nombre_c,
                    apellido_c:params.apellido_c,
                    cedula_c:params.cedula_c,
                    direccion_c:params.direccion_c,
                    telefono:params.telefono.telefono,
                    email:params.email

                }).then(contStored=>{
                    res.status(200).send({contStored:contStored});
                }).catch(err=>{
                    console.log(err);
                })
            }
        }).catch(err=>{
            console.log(err);
        })
    }
}

module.exports={
    getContribuyentes,
    contribuyenteStored,
    getContribuyente
}