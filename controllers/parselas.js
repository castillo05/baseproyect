'use strict'

const Sequelize=require('sequelize');
const env=require('../config');
const Parcela=require('../models/parselas');
const Contribuyente=require('../models/contribuyente');

// Asociando Roles con usuarios
// Parcela.hasMany(Contribuyente,{foreignKey:'id_contribuyente'});
// Contribuyente.belongsTo(Parcela,{foreignKey:'id_contribuyente'});


const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

 function parcelaCreate(req,res){
     let params = req.body;
   
     if (params.id_contribuyente=='' || params.anio_pagado=='' || params.cod_siscat=='' || params.jubilado=='' || params.descuento=='' || params.total=='' || params.pago1=='' ||
         params.pago2=='' || params.saldo=='' || params.nuevo=='' || params.aviso=='' ||    params.fecha_pago=='' || params.pn=='' || params.tipo_localidad=='' || params.barrio=='' || params.caserio=='')  {
        return res.status(200).send({message:'Llenar todos los campos debidamente!'});
     }
     Parcela.create({
         id_contribuyente:params.id_contribuyente,
         anio_pagado:params.anio_pagado,
         cod_siscat:params.cod_siscat,
         jubilado:params.jubilado,
         descuento:params.descuento,
         total:params.total,
         pago1:params.pago1,
         pago2:params.pago2,
         saldo:params.saldo,
         nuevo:params.nuevo,
         aviso:params.aviso,
         fecha_notificacion:params.fecha_notificacion,
         fecha_pago:params.fecha_pago,
         pn:params.pn,
         tipo_localidad:params.tipo_localidad,
         barrio:params.barrio,
         caserio:params.caserio

     }).then(parcelaStored=>{
         res.status(200).send({parcela:parcelaStored});
     }).catch(error=>{
         res.status(505).send({error:error});
     })
 }

 function getParcelas(req,res) {
    let id_contribuyente=req.params.id;
    Parcela.findAll({
      
        where:{
            id_contribuyente:id_contribuyente
        }
    }).then(parcelas=>{
        res.status(200).send({parcelas:parcelas});
    }).catch(error=>{
        res.status(505).send({error:error});
    })
 }

 function getParcela(req,res) {
     let id_parcela=req.params.id;
     Parcela.findOne({
         where:{
             id_parcela:id_parcela
         }
     }).then(parcela=>{
         res.status(200).send({parcela:parcela})
     }).catch(error=>{
         res.status(505).send({error:error});
     });
 }

 module.exports={
     parcelaCreate,
     getParcelas,
     getParcela
 }