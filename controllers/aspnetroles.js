'use strict'

const Sequelize=require('sequelize');
const Aspnetroles=require('../models/role');
const env=require('../config');

const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });

 function RoleStored(req,res){
    const R=new Aspnetroles;
    const params=req.body;

    if (params.Name=='' || params.Name==null || params.Discriminator=='' || params.Discriminator==null){
        return res.status(200).send({message:'Por favor rellene todos los campos'});
    }

    Aspnetroles.create({
        Name:params.Name,
        Enable:'1',
        Discriminator:params.Discriminator
    }).then(role=>{
        res.status(200).send({role:role});
    }).catch(err=>{
        res.status(500).send({err:err});
    });
 }

 module.exports={
     RoleStored
 }