'use strict'
const Sequelize=require('sequelize');
const Users=require('../models/aspnetusers');
const Aspnetroles=require('../models/role');
const env=require('../config');
const bcrypt=require('bcrypt');
const saltRounds = 10;
const jwt=require('../service/jwt');

const seq=require('../index');

// Asociando Roles con usuarios
Aspnetroles.hasMany(Users,{foreignKey:'RoleId'});
Users.belongsTo(Aspnetroles,{foreignKey:'RoleId'});


const sequelize=new Sequelize(env.database,env.username,env.password, {
    host:env.host,
     port:env.port,
     username:env.username,
     password:env.password,
     database:env.database,
     dialect:env.dialect
 });



 function UserStored(req,res) {
     const U=new Users; 
     var params=req.body;
    //  Validamos los campos para evitar errores de entrada de datos
    if (params.Mail=='' || params.Mail==null || params.LockoutEnabled=='' || params.LockoutEnabled==null || params.UserName=='' || params.UserName==null || params.Name=='' || params.Name==null || params.LastName=='' || params.LastName==null || params.Cedula=='' || params.Cedula==null) {
        res.status(200).send({message:'Llenar todos los campos!'});
    }else{ 
// Buscamos un usuario donde el email sea igual al introducido y si existe se lanza un sms.
            Users.findAll({
                where: {
                    Mail:params.Mail
                }
                }
            ).then(user=>{
                    if (user.length>=1) {
                        res.status(200).send({message:'Lo sentimos este Email ya esta registrado'});
// 
// Pero si no existe un email igual procedemos a crear el usuario
                    }else{
                        if (params.PasswordHash) {
                            bcrypt.hash(params.PasswordHash,saltRounds).then(function(hash){
                              Aspnetroles.findAll().then(roles=>{
                                roles.forEach(rol=>{
                                    if (rol.Name=='Usuario') {
                                        Users.create({
                                            Mail:params.Mail,
                                            PasswordHash:hash,
                                            PhoneNumber:params.PhoneNumber,
                                            LockoutEnabled:params.LockoutEnabled,
                                            UserName:params.UserName,
                                            Name:params.Name,
                                            LastName:params.LastName,
                                            Cedula:params.Cedula,
                                            RoleId:rol.Id
                                        }).then(userStored=>{
                                            res.status(200).send({userStored:userStored});
                                        }).catch(err=>{
                                           res.status(500).send({err});
                                        })
                                    }
                                });
                              }).catch(err=>{

                              });
                                
                               

                            }).catch(err=>{
                                console.log(err);
                            });
                        }else{
                            console.log('Introduzca la Contraseña');
                        }
                    }
            }).catch(err=>{
                console.log(err);
            });
        }
 }

 function SingIn(req,res) {
     var mail=req.body.Mail;
     var password=req.body.PasswordHash;

     sequelize.sync().then(()=>{
                    Users.findAll({
                            
                            where:{
                                Mail:mail
                            },        
                            include:[{
                                model:Aspnetroles,
                                as:''                       
                            }],
                                
                }).then(users=>{
                   
                    users.forEach(user => {
                        // res.status(200).send({User:user});
                             if (user.Mail==mail) {

                                if (password!='') {
                                        bcrypt.compare(password,user.PasswordHash) .then(function(check){
                                            if (check) {
                                                if (req.body.gethash) {
                                                    res.status(200).send({token:jwt.createToken(user)});
                                                } else {
                                                    res.status(200).send({user:user });
                                                }
                                            }else{
                                                res.status(200).send({message:'Contraseña Incorrecta'});
                                            }
                                        }).catch(err=>{
                                            res.status(505).send({err:err});
                                        });
                                }else if(password=='' || password==null){
                                    res.status(200).send({message:'Introduzca la contraseña'});
                                }
                            
                                
                            }else{
                                res.status(200).send({message:'Este email no esta registrado'});
                            }

                    });
                
                
                    
                }).catch(err=>{
                    res.status(505).send({message:err});
                }) 
    })
     
 }

 function GetUsers(req,res) {
    sequelize.sync().then(()=>{
            Users.findAll({
                include:[{
                    model:Aspnetroles,
                    as:''                       
                }]
            }).then(users=>{
            res.status(200).send({users:users});
        }).catch(err=>{
            res.status(404).send({Error:err});
        });
    });
   
 }

 function UserUpdate(req,res){
    var params=req.body;
    if (params.Mail=='' || params.Mail==null || params.LockoutEnabled=='' || params.LockoutEnabled==null || params.UserName=='' || params.UserName==null || params.Name=='' || params.Name==null || params.LastName=='' || params.LastName==null || params.Cedula=='' || params.Cedula==null) {
        res.status(200).send({message:'Llenar todos los campos!'});
    }else{ 
     Users.update({
        Mail:params.Mail,
        PhoneNumber:params.PhoneNumber,
        LockoutEnabled:params.LockoutEnabled,
        UserName:params.UserName,
        Name:params.Name,
        LastName:params.LastName,
        Cedula:params.Cedula
     },{
         where:{
             Id_User:req.params.Id_User
         }
     }).then((user)=>{
         
         if (user) return res.status(200).send({message:'Usurio Actualizado Satisfactoriamente'});
     }).catch(err=>{
         res.status(500).send({error:err});
     });
    }
 }

 function GetUser(req,res) {
     Users.sync().then(()=>{
          Users.findByPk(
              req.params.Id_User,
              
            ).then(user=>{
                res.status(200).send({user:user});
            }).catch(err=>{
                res.status(500).send({error:err});
            })
     })
    
 }

 module.exports={
     UserStored,
     SingIn,
     GetUsers,
     UserUpdate,
     GetUser
 }